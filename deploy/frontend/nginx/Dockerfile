FROM nginx:alpine as release

RUN rm /etc/nginx/conf.d/*

COPY ./nginx.conf  /etc/nginx/templates/nginx.conf.template
# Include location directive for Let's Encrypt ACME Challenge
COPY ./acme.conf   /etc/nginx/templates/ssl.conf.template
COPY ./attention.conf   /etc/nginx/templates/attention.conf.template
COPY ./backoffice.conf  /etc/nginx/templates/backoffice.conf.template
COPY ./videocall.conf   /etc/nginx/templates/videocall.conf.template

EXPOSE 80
EXPOSE 443

ENV NGINX_ENVSUBST_TEMPLATE_DIR=/etc/nginx/templates
ENV NGINX_ENVSUBST_TEMPLATE_SUFFIX=.template
ENV NGINX_ENVSUBST_OUTPUT_DIR=/etc/nginx/conf.d

RUN apk add python3 python3-dev py3-pip build-base musl-dev libffi-dev gcc openssl-dev cargo
RUN pip3 install wheel
RUN pip3 install pip --upgrade
RUN pip3 install certbot-nginx
RUN mkdir /etc/letsencrypt

COPY ./letsencrypt/ssl.pem   /etc/letsencrypt/ssl-dhparams.pem
COPY ./letsencrypt/ssl-options.conf   /etc/letsencrypt/options-ssl-nginx.conf

RUN mkdir /etc/letsencrypt/live
RUN mkdir /etc/letsencrypt/cuidemonos.us-east-1.elasticbeanstalk.com
RUN mkdir /etc/letsencrypt/app.cuidemonos.us-east-1.elasticbeanstalk.com
RUN mkdir /etc/letsencrypt/backoffice.cuidemonos.us-east-1.elasticbeanstalk.com
RUN mkdir /etc/letsencrypt/videollamadas.cuidemonos.us-east-1.elasticbeanstalk.com

COPY ./credentials/chain.pem   /etc/letsencrypt/live/cuidemonos.us-east-1.elasticbeanstalk.com/fullchain.pem
COPY ./credentials/key.pem   /etc/letsencrypt/live/cuidemonos.us-east-1.elasticbeanstalk.com/privkey.pem

COPY ./credentials/attention-chain.pem   /etc/letsencrypt/live/app.cuidemonos.us-east-1.elasticbeanstalk.com/fullchain.pem
COPY ./credentials/attention-key.pem   /etc/letsencrypt/live/app.cuidemonos.us-east-1.elasticbeanstalk.com/privkey.pem

COPY ./credentials/backoffice-chain.pem   /etc/letsencrypt/live/backoffice.cuidemonos.us-east-1.elasticbeanstalk.com/fullchain.pem
COPY ./credentials/backoffice-key.pem   /etc/letsencrypt/live/backoffice.cuidemonos.us-east-1.elasticbeanstalk.com/privkey.pem

COPY ./credentials/videocall-chain.pem   /etc/letsencrypt/live/videollamadas.cuidemonos.us-east-1.elasticbeanstalk.com/fullchain.pem
COPY ./credentials/videocall-key.pem   /etc/letsencrypt/live/videollamadas.cuidemonos.us-east-1.elasticbeanstalk.com/privkey.pem